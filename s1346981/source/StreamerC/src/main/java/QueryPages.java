import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCluster;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.SqlFieldsQuery;


import java.io.PrintWriter;
import java.util.List;

public class QueryPages {
    public static void main(String[] args) throws Exception {
        // Mark this cluster member as client.
        Ignition.setClientMode(true);
        PrintWriter writer;


        writer = new PrintWriter(args[1], "UTF-8");

        try (Ignite ignite = Ignition.start(args[0])) {
            IgniteCache<String, Long> stmCache = ignite.getOrCreateCache(CacheConfig.wordCache());
            IgniteCluster cluster = ignite.cluster();


	    // make query string to call
	    String queryString = "select _key, _val from Long order by _val desc limit 10";
            // Select top 10 words.
            SqlFieldsQuery top10Qry = new SqlFieldsQuery(queryString);

            // Query top 10 popular words every 1 seconds.
            while (true) {

		int totalNodes = cluster.metrics().getTotalNodes();

		//if only this query node and the server alive
		if(totalNodes == 2)
		{
		        List<List<?>> top10 = stmCache.query(top10Qry).getAll();
			int querySize = top10.size();

			//and results have been aggregated into the cache
			if(querySize > 0)
			{
				for(List<?> item : top10)
				{

				    String key = item.get(0).toString();
				    Long value = (Long) item.get(1);
				    System.out.printf("%d:%d:%s \n", System.currentTimeMillis() / 1000L, value, key);
				    try{

				        writer.printf("%d:%d:%s \n", System.currentTimeMillis() / 1000L, value, key);
				        writer.flush();

				    } catch (Exception e) {

				    }
				}
				//in this case we know we can exit
				System.exit(0);
			}
		}
                Thread.sleep(10000);
            }
        }
    }
}
