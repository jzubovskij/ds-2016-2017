#!/bin/bash	


export JAVA_HOME=/usr/lib/jvm/java-1.8.0
IGNITE_HOME=/afs/inf.ed.ac.uk/user/s13/s1346981/Downloads/apache-ignite-fabric-1.7.0-bin
#IGNITE_HOME already set as per Piazza answer


IGNITE_SCRIPT=$IGNITE_HOME/bin

IGNITE_CONFIG=./dependency/example-ignite-no-discovery.xml
      
DEPENDENCIES_A=./dependency/lib/*:./build/Streamer-A-1.0-SNAPSHOT.jar:.
DEPENDENCIES_B=./dependency/lib/*:./build/Streamer-B-1.0-SNAPSHOT.jar:.
DEPENDENCIES_C=./dependency/lib/*:./build/Streamer-C-1.0-SNAPSHOT.jar:.

LOG_A=./log/log-partA.txt
LOG_B=./log/log-partB.txt
LOG_C=./log/log-partC.txt


if [ "$1" = "compile" ]; then

	
	mvn -f ./source/StreamerA/pom.xml package
	mvn -f ./source/StreamerB/pom.xml package
	mvn -f ./source/StreamerC/pom.xml package

	
elif [ "$1" = "partA" ]; then

	gnome-terminal -e "$IGNITE_SCRIPT/ignite.sh $IGNITE_CONFIG"
	sleep 2
	gnome-terminal -e "java -cp $DEPENDENCIES_A  QueryPages  "$IGNITE_CONFIG" "$LOG_A" " 
	sleep 2
	gnome-terminal -e "java -cp $DEPENDENCIES_A  StreamPages "$IGNITE_CONFIG" "$2" "
		

elif [ "$1" = "partB" ]; then

	gnome-terminal -e "$IGNITE_SCRIPT/ignite.sh $IGNITE_CONFIG"
	sleep 2
	gnome-terminal -e "java -cp $DEPENDENCIES_B  QueryPages  "$IGNITE_CONFIG" "$LOG_B" " 
	sleep 2
	gnome-terminal -e "java -cp $DEPENDENCIES_B  StreamPages "$IGNITE_CONFIG" "$2" "
	gnome-terminal -e "java -cp $DEPENDENCIES_B  StreamPages "$IGNITE_CONFIG" "$3" "
	gnome-terminal -e "java -cp $DEPENDENCIES_B  StreamPages "$IGNITE_CONFIG" "$4" "

elif [ "$1" = "partC" ]; then
	
	gnome-terminal -e "$IGNITE_SCRIPT/ignite.sh $IGNITE_CONFIG"
	sleep 2
	gnome-terminal -e "java -cp $DEPENDENCIES_C  QueryPages  "$IGNITE_CONFIG" "$LOG_C" "
	sleep 2  
	gnome-terminal -e "java -cp $DEPENDENCIES_C  StreamPages "$IGNITE_CONFIG" "$2" "
	gnome-terminal -e "java -cp $DEPENDENCIES_C  StreamPages "$IGNITE_CONFIG" "$3" "
	gnome-terminal -e "java -cp $DEPENDENCIES_C  StreamPages "$IGNITE_CONFIG" "$4" "	

fi 
