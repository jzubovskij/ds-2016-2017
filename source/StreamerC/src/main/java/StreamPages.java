import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteDataStreamer;
import org.apache.ignite.Ignition;
import org.apache.ignite.stream.StreamTransformer;


import javax.cache.processor.EntryProcessorException;
import javax.cache.processor.MutableEntry;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;


class StreamingProcessor extends StreamTransformer<String, Long> {
    @Override
    public Object process(MutableEntry<String, Long> e, Object... arg) throws EntryProcessorException {

        //get value passed in as argument
        Long count = (Long) arg[0];
        //if new value, just add argument, else add argument to existing one
        count = (e.getValue() == null ? count : count + e.getValue());
        //record the change
        e.setValue(count);
        return null;
    }
}

public class StreamPages {


    public static void main(String[] args) throws Exception {
        // Nodes is a client
        Ignition.setClientMode(true);
	
        try (Ignite ignite = Ignition.start(args[0])) {
            //create the streaming cache
            IgniteCache<String, Long> stmCache = ignite.getOrCreateCache(CacheConfig.wordCache());

            // Create a page count streamer
            try (IgniteDataStreamer<String, Long> stmr = ignite.dataStreamer(stmCache.getName())) {
                // Allow data updates
                stmr.allowOverwrite(true);

                // make the stream transformer
                stmr.receiver(new StreamingProcessor());

                Path path = Paths.get(args[1]);

                // Read words from a text file.
                try (Stream<String> lines = Files.lines(path)) {
                    lines.forEach(line -> {
                        String[] tokens = line.split(" ");
                        //account for incorrect lines
                        if (tokens.length > 2) {
                            String page = tokens[1];
                            Long count = Long.parseLong(tokens[2]);

			     //only stream wikibooks
			     if(tokens[0].endsWith(".b")){
                            	//add data to the streamer
                            	stmr.addData(page, count);
			    }	
                        }


                    });
                }
                stmr.close();

            }
        }
    }
}
