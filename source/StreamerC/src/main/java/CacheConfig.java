
import org.apache.ignite.cache.affinity.AffinityUuid;
import org.apache.ignite.cache.eviction.fifo.FifoEvictionPolicy;
import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.configuration.FactoryBuilder;
import javax.cache.expiry.CreatedExpiryPolicy;
import javax.cache.expiry.Duration;

import static java.util.concurrent.TimeUnit.SECONDS;
public class CacheConfig {
    public static CacheConfiguration<String, Long> wordCache() {
	// making the configuration for the page cache
        CacheConfiguration<String, Long> configuration = new CacheConfiguration<>("page_cache");

        // index by page name and count
        configuration.setIndexedTypes(String.class, Long.class);

        return configuration;
    }
}
