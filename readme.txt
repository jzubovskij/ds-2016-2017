Distributed System coursework. 
###################################
# RUNNING FUNCTIONS
###################################

1) run.sh compile                                #compiles parts A,B and C
2) run.sh partA file_name                        #runs partA and streams the specified file's data
3) run.sh partB file_name1 file_name2 file_name3 #runs partB and streamer N streams file specified by file_nameN's data
4) run.sh partC file_name1 file_name2 file_name3 #runs partC and streamer N streams file specified by file_nameN's data

###################################
# RUNNING NOTES AND DESIGN DECISIONS
###################################

1) Before launching any partN, please ensure the previous ignite cluster instance is closed as otherwise cache might
contain entries from previous runs. This is especially important for Part C as there is no eviction policy
present.

2) Moreover, please make sure that for all parts IGNITE is running with the configuration: example-ignite-no-discovery.xml

3) As per Piazza posts I assume IGNITE_HOME is set 

4) I set the JAVA_HOME, however, if for one reason or another on the computer it is run on the path is incorrect, please change it to the appropriate path to indicate the location of JAVA 8

5) The reason for IGNITE processes in new terminal windows is that it allows more independent debugging and data 
monitoring if needed and the decision to not make N threads, instead make N windows. Moreover, it is truly a distributed 
computation where we can launch the streamers on different nodes, also distributing the load.

6) Task C only sends out Wikibooks to remove the need for a complex query and reduces network strain by only sending relevant information

7) The run scrip also intorduces delay (via sleep) between the launching of the server node, query node and the streamer. The delay between server and query node is so that it instantly connects. The delay between the query and streamers is argumented by the fact that without the delay the streamers can launch before the query node, therefore the query node would have the appropriate data on the 1st query. This, however, is important only for parts A and B as in C we only do one query at the end.

8) Please run run.sh only from the directory it is located in (originally, as seen in submitted folder) because the paths for the dependencies and tother (project build) jars are set with the assumption that it is launched exactly as such.

###################################
# FILES USED 
###################################


1) File 1 : https://dumps.wikimedia.org/other/pagecounts-raw/2016/2016-01/pagecounts-20160102-210000.gz
2) File 2 : https://dumps.wikimedia.org/other/pagecounts-raw/2016/2016-05/pagecounts-20160502-210000.gz 
3) File 3 : https://dumps.wikimedia.org/other/pagecounts-raw/2016/2016-08/pagecounts-20160801-210000.gz 

File 1 used to test part A, Files 1,2,3 used to test parts A and B, passing them as arguments to the 
run.sh script (indicated by $2, $3 and $4 in the script). To avoid any confusion, the exact way it was passed
through the command line arguments made the corresponding variables (as per above) be equal to  

	1. $2 = File 1
	2. $3 = File 2
	3. $4 = File 3



